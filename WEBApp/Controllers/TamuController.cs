﻿using Microsoft.AspNetCore.Mvc;
using WEBApp.Provider.Tamus;
using WEBApp.ViewModel.Base;
using WEBApp.ViewModel.Tamus;

namespace WEBApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TamuController : Controller
    {
        private readonly ITamuProvider _tamuProvider;
        private readonly AjaxViewModel response;
        public TamuController(ITamuProvider tamuProvider)
        {
            _tamuProvider = tamuProvider;
            response = new AjaxViewModel();
        }
        
        [HttpGet]
        [Route("GetIndex/{page}")]
        public IndexTamuViewModel Index(int page)
        {
            var model = _tamuProvider.GetIndex(page);
            return model;
        }


        [HttpGet("[action]/{id}")]
        public IActionResult GetEdit(int id)
        {
            
            try
            {
                var data = _tamuProvider.GetEdit(id);
                response.SetValues(Ok().StatusCode, true , data, "Success");
            }
            catch (Exception e)
            {
                response.SetValues(false, null, e.ToString());
            }
            return Ok(response);
        }

        [HttpPost("[action]")]
        public IActionResult Create([FromBody] UpsertTamuViewModel model)
        {
            try 
            {
                var data = _tamuProvider.Save(model);
                if (data.IsSucceed)
                {
                    response.SetValues(Ok().StatusCode, true, data.Data, data.Message);
                }
                else
                {
                    response.SetValues(BadRequest().StatusCode, false, null, data.Message);
                }

            }
            catch (Exception e)
            {
                response.SetValues(false, null, e.ToString());
            }
            return Ok(response);
        }        
        
        [HttpPut("[action]")]
        public IActionResult Edit([FromBody] UpsertTamuViewModel model)
        {
            try 
            {
                var data = _tamuProvider.Save(model);
                if (data.IsSucceed)
                {
                    response.SetValues(Ok().StatusCode, true, data.Data, data.Message);
                }
                else
                {
                    response.SetValues(BadRequest().StatusCode, false, null, data.Message);
                }

            }
            catch (Exception e)
            {
                response.SetValues(false, null, e.ToString());
            }
            return Ok(response);
        }

        [HttpDelete("[action]/{id}")]
        public IActionResult Delete(int id)
        {
            var model = _tamuProvider.Delete(id);
            try
            {
                if (model.IsSucceed)
                {
                    response.SetValues(Ok().StatusCode, true, model.Data, model.Message);
                }
                else
                {
                    response.SetValues(BadRequest().StatusCode, false, null, model.Message);
                }

            }
            catch (Exception ex)
            {
                response.SetValues(false, null, ex.ToString());
            }

            return Ok(response);
        }
    }
}
