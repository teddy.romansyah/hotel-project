﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.ViewModel.Tamus
{
    public class GridTamuViewModel
    {
        public int Id { get; set; }
        public string? NomorRegister { get; set; }
        public string? NamaDepan { get; set; }
        public string? NamaBelakang { get; set; }
        public DateTime? TanggalLahir { get; set; }
        public string? TempatLahir { get; set; }
        public string? JenisKelamin { get; set; }
        public string? NomorKtp { get; set; }

    }
}
