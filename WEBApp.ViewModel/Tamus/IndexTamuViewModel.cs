﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.ViewModel.Tamus
{
    public class IndexTamuViewModel
    {
        public int TotalData { get; set; }
        public int TotalHalaman { get; set; }
        public IEnumerable<GridTamuViewModel> GridTamu { get; set; }
    }
}
