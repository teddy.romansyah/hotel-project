﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.ViewModel.Base
{
    public class ProcessResult
    {
        public object Data { get; set; }
        public string Message { get; set; }
        public bool IsSucceed { get; set; }

        public ProcessResult()
        {

        }

        public void InsertSucceed()
        {
            this.IsSucceed = true;
            this.Message = "Data Berhasil Disimpan.";
        }

        public void UpdateSucceed()
        {
            this.IsSucceed = true;
            this.Message = "Data Berhasil Disimpan.";
        }

        public void DeleteSucceed()
        {
            this.IsSucceed = true;
            this.Message = "Data Berhasil dihapus";
        }
        public void DataIsExist(string id)
        {
            this.IsSucceed = true;
            this.Message = id + " Sudah Ada";
        }

        public void ProcessSucceed(string message)
        {
            this.IsSucceed = true;
            this.Message = message;
        }

        public void ProcessFailed(string message)
        {
            this.IsSucceed = false;
            this.Message = message;
        }
    }
}
