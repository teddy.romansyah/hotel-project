﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.ViewModel.Base
{
    public class AjaxViewModel
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public bool IsSucceed { get; set; }
        public object ReturnObject { get; set; }

        public void SetValues(bool isSuccess, object data, string message)
        {
            this.IsSucceed = isSuccess;
            this.ReturnObject = data;
            this.Message = message;
        }        
        
        public void SetValues(int code,bool isSuccess, object data, string message)
        {
            this.Code = code;
            this.IsSucceed = isSuccess;
            this.ReturnObject = data;
            this.Message = message;
        }
    }
}
