﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.Provider
{
    public abstract class BaseProvider
    {
        protected const int dataPerPage = 5;

        protected int GetSkipValue(int page)
        {
            return (page - 1) * dataPerPage;
        }

        protected int GetTotalPage(int totalData)
        {
            return (int)Math.Ceiling(totalData / (decimal)dataPerPage);
        }
    }
}
