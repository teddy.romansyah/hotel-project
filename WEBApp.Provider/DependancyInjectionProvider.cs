﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WEBApp.Provider.Tamus;

namespace WEBApp.Provider
{
    public static class DependancyInjectionProvider
    {
        public static void AddProvider(this IServiceCollection service)
        {
            service.AddScoped<ITamuProvider, TamuProvider>();
        } 
    }
}
