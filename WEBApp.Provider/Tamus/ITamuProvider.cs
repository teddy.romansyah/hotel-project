﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEBApp.ViewModel.Base;
using WEBApp.ViewModel.Tamus;

namespace WEBApp.Provider.Tamus
{
    public interface ITamuProvider
    {
        public IndexTamuViewModel GetIndex(int page);
        public UpsertTamuViewModel GetEdit(object id);
        public ProcessResult Save(UpsertTamuViewModel model);
        public ProcessResult Delete(object id);
    }
}
