﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;
using WEBApp.DataAccess.Models;
using WEBApp.ViewModel.Base;
using WEBApp.ViewModel.Tamus;

namespace WEBApp.Provider.Tamus
{
    public class TamuProvider : BaseProvider, ITamuProvider
    {
        private readonly SilverCoastHotelContext _context;
        
        public TamuProvider(SilverCoastHotelContext context)
        {
            _context = context;
        }

        public ProcessResult Delete(object id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var tamu = _context.Tamus.SingleOrDefault(a => a.Id == (int)id);
                if (tamu == null)
                {
                    result.ProcessFailed("Data tidak ada dalam Database!!");
                    return result;
                }
                result.Data = tamu;

                _context.Remove(tamu);
                _context.SaveChanges();

                result.DeleteSucceed();
                return result;

            }
            catch (Exception ex)
            {
                result.ProcessFailed(ex.Message);
                return result;
            }
        }


        public IEnumerable<GridTamuViewModel> GetTamu()
        {
            var tamu = (from t in _context.Tamus
                        join j in _context.MstJenisKelamins on t.JenisKelamin equals j.Id
                        select new GridTamuViewModel
                        {
                            Id = t.Id,
                            NomorRegister = t.NomorRegister,
                            NamaDepan = t.NamaDepan,
                            NamaBelakang = t.NamaBelakang,
                            JenisKelamin = j.Description,
                            NomorKtp = t.NomorKtp,
                            TanggalLahir = t.TanggalLahir,
                            TempatLahir = t.TempatLahir
                        }).AsEnumerable();

            return tamu;
        }

        public IndexTamuViewModel GetIndex(int page)
        {
            var data = GetTamu().ToList();

            var model = new IndexTamuViewModel
            {
                TotalData = data.Count,
                TotalHalaman = GetTotalPage(data.Count),
                GridTamu = data.Skip(GetSkipValue(page)).Take(dataPerPage),
            };

            return model;
        }



        public UpsertTamuViewModel GetEdit(object id)
        {
            try
            {
                var tamu = _context.Tamus.Where(a => a.Id == (int)id).Select(a => new UpsertTamuViewModel
                {
                    Id = (int)id,
                    NomorRegister = a.NomorRegister,
                    NamaDepan = a.NamaDepan,
                    NamaBelakang = a.NamaBelakang,
                    JenisKelamin = a.JenisKelamin,
                    TempatLahir = a.TempatLahir,
                    TanggalLahir = a.TanggalLahir,
                    NomorKtp = a.NomorKtp,
                    CreatedBy = a.CreatedBy,
                    CreatedDate = a.CreatedDate
                });

                return tamu.SingleOrDefault();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ProcessResult Save(UpsertTamuViewModel model)
        {
            ProcessResult result = new ProcessResult();
            Tamu guest = null;
            try
            {
                guest = _context.Tamus.SingleOrDefault(a => a.Id == model.Id);

                if (guest == null)
                {
                    guest = new Tamu();
                    guest.NomorRegister = model.NomorRegister;
                    guest.NamaDepan = model.NamaDepan;
                    guest.NamaBelakang = model.NamaBelakang;
                    guest.TanggalLahir = model.TanggalLahir;
                    guest.TempatLahir = model.TempatLahir;
                    guest.NomorKtp = model.NomorKtp;
                    guest.CreatedBy = "SYSTEM";
                    guest.CreatedDate = DateTime.Now;
                    result.Data = guest;

                    _context.Tamus.Add(guest);
                    result.InsertSucceed();
                }
                else
                {
                    guest.NomorRegister = model.NomorRegister;
                    guest.NamaDepan = model.NamaDepan;
                    guest.NamaBelakang = model.NamaBelakang;
                    guest.TanggalLahir = model.TanggalLahir;
                    guest.TempatLahir = model.TempatLahir;
                    guest.NomorKtp = model.NomorKtp;

                    guest.UpdatedDate = DateTime.Now;
                    guest.UpdatedBy = model.UpdatedBy;

                    result.Data = guest;
                    result.UpdateSucceed();

                }
                _context.SaveChanges();
                return result;
            }
            catch (Exception ex)
            {
                result.ProcessFailed(ex.Message.ToString());
                return result;
            }
        }

    }
}
