﻿using System;
using System.Collections.Generic;

namespace WEBApp.DataAccess.Models
{
    public partial class Tamu
    {
        public int Id { get; set; }
        public string NomorRegister { get; set; } = null!;
        public string? NamaDepan { get; set; }
        public string? NamaBelakang { get; set; }
        public DateTime? TanggalLahir { get; set; }
        public string? TempatLahir { get; set; }
        public int? JenisKelamin { get; set; }
        public string? NomorKtp { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; } = null!;
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
    }
}
