﻿using System;
using System.Collections.Generic;

namespace WEBApp.DataAccess.Models
{
    public partial class Account
    {
        public int Id { get; set; }
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public int RoleId { get; set; }

        public virtual MstRole Role { get; set; } = null!;
    }
}
