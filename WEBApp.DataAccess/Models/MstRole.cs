﻿using System;
using System.Collections.Generic;

namespace WEBApp.DataAccess.Models
{
    public partial class MstRole
    {
        public MstRole()
        {
            Accounts = new HashSet<Account>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;

        public virtual ICollection<Account> Accounts { get; set; }
    }
}
