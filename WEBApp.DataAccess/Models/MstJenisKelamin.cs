﻿using System;
using System.Collections.Generic;

namespace WEBApp.DataAccess.Models
{
    public partial class MstJenisKelamin
    {
        public int Id { get; set; }
        public string? Nama { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; } = null!;
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
    }
}
